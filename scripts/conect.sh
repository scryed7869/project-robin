
#sleep 300
((count = 60))                           # Maximum number to try.

while [[ 1 -ne 2 ]] ; do
sleep 5
((count = 5))  
while [[ $count -ne 0 ]] ; do
    ping -c 1 8.8.8.8     > /dev/null 2>&1                  # Try once.
    rc=$?
    
    if [[ $rc -eq 0 ]] ; then
        ((count = 1))                    # If okay, flag loop exit.
    else
        sleep 1                          # Minimise network storm.
    fi
    ((count = count - 1))                # So we don't go forever.
done

if [[ $rc -eq 0 ]] ; then                # Make final determination.
    echo 'say The internet is back up.'
    
else
    echo 'say Timeout.'
    sudo /home/pi/Documents/homepi/home-surveillance-with-node-and-raspberry-pi/turn_on_4g_script.sh
fi

done
