const OpenTok = require('opentok');
const puppeteer = require('puppeteer');
const express = require('express');
const path = require('path');
const https = require('https');
const fs = require('fs');
const dotenv = require('dotenv');
const gpio = require('onoff').Gpio;
const fetch = require('node-fetch')

var  {PythonShell} = require('python-shell');

const app = express();
const pir = new gpio(18, 'in', 'both');
const ngrok = require('ngrok');
const db = require('./models/index');

const shell = require('shelljs') ;

var enable4g   = true;
var enablegrok = true;
var obdgpsprocess = false;
var readingobd = false;
var readinggps = true;
var obddata;
var gpsnmea;


const key = 47051204;
const secret = 'c57bb252c510483c72caea692e9d6fbe64f15633';

dotenv.config();

/*const opentok = new OpenTok(
  process.env.VONAGE_VIDEO_API_KEY,
  process.env.VONAGE_VIDEO_API_SECRET,
);*/
const opentok = new OpenTok(
  key,
  secret,
);


let canCreateSession = true;
let session = null;
let url = null;

async function startPublish() {
  const browser = await puppeteer.launch({
    headless: false,
    executablePath: 'chromium-browser',
    ignoreHTTPSErrors: true,
    args: [
      '--ignore-certificate-errors',
      '--use-fake-ui-for-media-stream',
      '--disable-web-security',
      '--no-user-gesture-required',
      '--autoplay-policy=no-user-gesture-required',
      '--allow-http-screen-capture',
      '--enable-experimental-web-platform-features',
      '--auto-select-desktop-capture-source=Entire screen',
    ],
  });
  const page = await browser.newPage();

  const context = browser.defaultBrowserContext();
  await context.overridePermissions('https://localhost:3000', ['camera', 'microphone']);

  await page.goto('https://localhost:3000/serve');

  async function closeSession(currentPage, currentBrowser) {
    console.log('Time limit expired. Closing stream');
    await currentPage.close();
    await currentBrowser.close();

    if (session !== null) {
      session.update({
        active: false
      });
    }
  }

  let sessionDuration = parseInt(process.env.VIDEO_SESSION_DURATION);
  let sessionExpiration = sessionDuration + 10000;

  setTimeout(closeSession, sessionDuration, page, browser);
  setTimeout(() => { canCreateSession = true; }, sessionExpiration);
}


async function startPublish2() {
  const browser = await puppeteer.launch({
    headless: false,
    executablePath: 'chromium-browser',
    ignoreHTTPSErrors: true,
    args: [
      '--ignore-certificate-errors',
      '--use-fake-ui-for-media-stream',
      '--disable-web-security',
      '--no-user-gesture-required',
      '--autoplay-policy=no-user-gesture-required',
      '--allow-http-screen-capture',
      '--enable-experimental-web-platform-features',
      '--auto-select-desktop-capture-source=Entire screen',
    ],
  });
  const page = await browser.newPage();

  const context = browser.defaultBrowserContext();
  await context.overridePermissions('https://localhost:3000', ['camera', 'microphone']);

  await page.goto('https://localhost:3000/serve2');

  async function closeSession(currentPage, currentBrowser) {
    console.log('Time limit expired. Closing stream');
    await currentPage.close();
    await currentBrowser.close();

    if (session !== null) {
      session.update({
        active: false
      });
    }
  }

  let sessionDuration = parseInt(process.env.VIDEO_SESSION_DURATION);
  let sessionExpiration = sessionDuration + 10000;

  setTimeout(closeSession, sessionDuration, page, browser);
  setTimeout(() => { canCreateSession = true; }, sessionExpiration);
}


function createSessionEntry(newSessionId) {
  db.Session
    .create({
      sessionId: newSessionId,
      active: true,
    })
    .then((sessionRow) => {
      session = sessionRow;

      return sessionRow.id;
    });
}

function obdprocess() {
    setInterval(function(){
      if(readingobd == false){
          readingobd = true;
          console.log("reading OBD");
          PythonShell.run('/home/pi/Documents/homepi/home-surveillance-with-node-and-raspberry-pi/her2.py',null, function (err, results) {
          if (err) console.log( err);
          console.log('results: %j', results);
          obddata = results;
          //console.log('finished');
          //console.log(new Date().getTime())
          readingobd = false;
         });
      }else{
        console.log("reading OBD false");
      }

      if(readinggps == false){
          readinggps = true;
          console.log("reading OBD");
          PythonShell.run('/home/pi/Documents/homepi/home-surveillance-with-node-and-raspberry-pi/gps.py',null, function (err, results) {
          if (err) throw err;
          console.log('results: %j', results);
          console.log('finished');
          gpsnmea = results;
          readinggps = false;
          });
      }else{
        console.log("reading GPS false");
      }
    }, 100);

    PythonShell.run('/home/pi/Documents/homepi/home-surveillance-with-node-and-raspberry-pi/gpson.py',null, function (err, results) {
//    if (err) throw err;
    if (err) console.log(`Error creating session:${error}`);
    console.log('results: %j', results);
    console.log('finished');
    gpsnmea = results;
    readinggps = false;
    });
}

async function createSession() {
  opentok.createSession({ mediaMode: 'routed' }, (error, session) => {
    if (error) {
      console.log(`Error creating session:${error}`);

      return null;
    }

    createSessionEntry(session.sessionId);

    startPublish();
    //startPublish2();
    console.log(`start publishiinnng`);

    return null;
  });
}

async function connectNgrok() {
  let url = await ngrok.connect({
    proto: 'http',
    addr: 'https://localhost:3000',
    region: 'eu',
    // The below examples are if you have a paid subscription with Ngrok where you can specify which subdomain to use
    // And add the location of your configPath. For me, it was gregdev which results in https://gregdev.eu.ngrok.io, a reserved subdomain
   subdomain: 'robinn',
    configPath: '/home/pi/.ngrok2/ngrok.yml',
    onStatusChange: (status) => { console.log(`Ngrok Status Update:${status}`); },
    onLogEvent: (data) => { console.log(data); },
  });

  //const post = bent('http://192.168.0.101:8080/', 'POST', 'json', 200);
  //const response = await post('url', {name: 'bmw', wheels: 4});
  //fetch('http://192.168.0.100:8080/url', { method: 'POST', body: 'a=1' })
 ///   .then(res => res.json()) // expecting a json response
 //   .then(json => console.log(json));
  fs.writeFile('/home/pi/Documents/homepi/home-surveillance-with-node-and-raspberry-pi/public/config/config.txt', url, (err) => {
   // if (err) throw err;
 if (err) console.log( err);
 console.log('The file has been saved!');
  });

}

async function startServer() {
  const port = 3000;

  app.use(express.static(path.join(`${__dirname}/public`)));
  app.get('/serve', (req, res) => {
    res.sendFile(path.join(`${__dirname}/public/server.html`));
  });
  app.get('/serve2', (req, res) => {
    res.sendFile(path.join(`${__dirname}/public/server2.html`));
  });

  app.get('/client', (req, res) => {
    res.sendFile(path.join(`${__dirname}/public/client.html`));
  });

  app.get('/health', (req, res) => {
    res.send("good");
  });

  app.get('/ngrok', (req, res) => {
    connectNgrok();
    res.send("good one");
  });

  app.get('/dongle', (req, res) => {
    shell.exec('/home/pi/Documents/homepi/home-surveillance-with-node-and-raspberry-pi/turn_on_4g_script.sh')
    res.sendFile(path.join(`${__dirname}/public/client.html`));
  });

  app.get('/gpsnmeaold', (req, res) => {
    PythonShell.run('gps.py',null, function (err, results) {
    //if (err) throw err;
     if (err) console.log( err);
	console.log('results: %j', results);
    console.log('finished');
    res.send(results);
    });
   });

  app.get('/gpsnmea', (req, res) => {
    res.send(gpsnmea);
   });


  app.get('/gpson', (req, res) => {
    PythonShell.run('/home/pi/Documents/homepi/home-surveillance-with-node-and-raspberry-pi/gpson.py',null, function (err, results) {
   // if (err) throw err;
	 if (err) {
	console.log( err);
	return;
		}
    console.log('results: %j', results);
    console.log('finished');
    res.send(results);
   });

});



  app.get('/battery', (req, res) => {
    PythonShell.run('her.py',null, function (err, results) {
    //if (err) throw err;
     if (err) console.log( err);
    console.log('results: %j', results);
    console.log('finished');
    res.send(results);
   });

});


  app.get('/readobdold', (req, res) => {
    console.log("in"+new Date().getTime())
    PythonShell.run('/home/pi/Documents/homepi/home-surveillance-with-node-and-raspberry-pi/her2.py',null, function (err, results) {
    if (err) console.log( err);
    console.log('results: %j', results);
    console.log('finished');
    console.log(new Date().getTime())
    res.send(results);
   });

});

  app.get('/readobd', (req, res) => {
    res.send(obddata);
   });


  app.get('/gpsstatus', (req, res) => {
    PythonShell.run('gpsstatus.py',null, function (err, results) {
    if (err) throw err;
    console.log('results: %j', results);
    console.log('finished');

    res.send(results);
   });

});

 app.get('/clientt', (req, res) => {
    if ( canCreateSession === true) {
    canCreateSession = false;
    console.log('Motion has been detected');

    createSession();
  }
 res.sendFile(path.join(`${__dirname}/public/client.html`));
  });


  app.get('/get-details', (req, res) => {
    db.Session.findAll({
      limit: 1,
      where: {
        active: true,
      },
      order: [['createdAt', 'DESC']],
    }).then((entries) => res.json({
      sessionId: entries[0].sessionId,
      token: opentok.generateToken(entries[0].sessionId),
      //apiKey: process.env.VONAGE_VIDEO_API_KEY,
      apiKey: key,
    }));
  });

  const httpServer = https.createServer({
   // key: fs.readFileSync('./key.pem'),
   // cert: fs.readFileSync('./cert.pem'),
   // passphrase: 'root',
    key: fs.readFileSync('/home/pi/Documents/homepi/home-surveillance-with-node-and-raspberry-pi/key.pem'),
    cert: fs.readFileSync('/home/pi/Documents/homepi/home-surveillance-with-node-and-raspberry-pi/cert.pem'),
    passphrase: 'root',
  }, app);

  httpServer.listen(port, (err) => {
    if (err) {
      return console.log(`Unable to start server: ${err}`);
    }

    //connectNgrok();
    shell.exec('./turn_on_4g_script.sh')
    //enable qualcomm and connect to ngrok
    if(enable4g){
      setTimeout(function() {
      console.log('I am a timeoutttt fuc  adss');
      //connectNgrok();
      var i;
      for(i=0;i<3;i++)
      	shell.exec('/home/pi/Documents/homepi/home-surveillance-with-node-and-raspberry-pi/turn_on_4g_script.sh')
      	//connectNgrok();
      //obdprocess();
      }, 20000);
    }
     //connect to ngrok online without starting qualcomm conection
    if(enablegrok){
      connectNgrok();
    }
     //connect to ngrok online without starting qualcomm conection
    if(obdgpsprocess){
      obdprocess();
    }

    setTimeout(function() {
      console.log('I am a timeoutttt fuc  padss');
      //connectNgrok();
      var i;
      for(i=0;i<0;i++)
      	shell.exec('/home/pi/Documents/homepi/home-surveillance-with-node-and-raspberry-pi/turn_on_4g_script.sh')
      //connectNgrok();
    }, 120000);
    //}, 1000);

    return true;
  });
}

// Triggers the whole process of creating a session, adding the the session id to the database.
// Opens a headless mode for the publisher view.
// Will send a text message.
startServer();

pir.watch((err, value) => {
  if (value === 1 && canCreateSession === true) {
    canCreateSession = false;
    console.log('Motion has been detected');

    createSession();
  }
});
