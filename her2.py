import time
import serial


try:
     ser = serial.Serial(

               port='/dev/ttyUSB0',
               baudrate = 38400,
               parity=serial.PARITY_NONE,
               stopbits=serial.STOPBITS_ONE,
               bytesize=serial.EIGHTBITS,
               timeout=0.1
          )
except serial.serialutil.SerialException:
    print ("Arduino not connected")
    quit
counter=0
try:
     while 1:
          #engine coolant temperature
          #ser.write(str.encode('01 05\r'))
          ser.write(str.encode('01 05\r 01 0C\r 01 0D\r 01 11\r 01 42\r AT RV\r'))
          readedText = ser.readline()
          print(readedText)
          break
except:
  print("An exception occurred",sys.exc_info()[0])
finally:
     ser.close
